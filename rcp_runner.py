#!/usr/bin/env python3
"""Script to run RCP application"""

import logging
import logging.handlers
import src.rcp as rcp

import OPi.GPIO as GPIO


LOG_FILENAME = "/tmp/rcservice.log"
LOG_LEVEL = logging.DEBUG
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(LOG_LEVEL)
HANDLER = logging.handlers.TimedRotatingFileHandler(LOG_FILENAME, when="midnight", backupCount=3)
FORMATTER = logging.Formatter('%(asctime)s %(levelname)-8s %(message)s')
HANDLER.setFormatter(FORMATTER)
LOGGER.addHandler(HANDLER)

GPIO.cleanup()


while True:
    try:
        PROGRAM = rcp.Program(LOGGER)
        PROGRAM.main()
    except Exception as ex:
        LOGGER.log(logging.ERROR, ex)
