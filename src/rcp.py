#!/usr/bin/env python3
"""Main file for reading Mifare cards"""

import sys
import signal
import binascii
import json
import logging
import time
import requests

import src.lib.PN532SPI as PN532SPI
import OPi.GPIO as GPIO

CONFIG = "DEFAULT"

class Program(object):
    blue_led = None
    mode = False
    def __init__(self, logger):
        self.logger = logger
        with open('/usr/local/bin/rcp/config.json', 'r') as f:
            self._config = json.load(f)
        self._mifare = Mifare(
            self._config[CONFIG]['CS'],
            self._config[CONFIG]['MISO'],
            self._config[CONFIG]['MOSI'],
            self._config[CONFIG]['SCLK'],
            self.logger)
        signal.signal(signal.SIGINT, self.exit_handler)
        self.green_led = OutputDevice(self._config[CONFIG]['GREEN_LED'], OutputDevice.LOW)
        self.red_led = OutputDevice(self._config[CONFIG]['RED_LED'], OutputDevice.LOW)
        # "static" for callback to change mode
        Program.blue_led = OutputDevice(self._config[CONFIG]['BLUE_LED'], OutputDevice.LOW)
        self.buzzer = OutputDevice(self._config[CONFIG]['BUZZER'], OutputDevice.LOW)
        self.logger.log(logging.DEBUG, "Program started")
        GPIO.setup(self._config[CONFIG]['BUTTON'], GPIO.IN)
        GPIO.add_event_detect(self._config[CONFIG]['BUTTON'], GPIO.RISING, callback=Program.change_mode)

    def setup(self):
        self._mifare.setup()
        self.setup_pins()

    @staticmethod
    def change_mode(channel):
        if Program.mode:
            Program.mode = False
            Program.blue_led.off()
        else:
            Program.mode = True
            Program.blue_led.on()

    def main(self):
        try:
            self.setup()
            self.blue_led.off()
        except Exception as ex:
            self.logger.log(logging.ERROR, ex)
            sys.exit(1)
        while True:
            try:
                self.red_led.off()
                self.green_led.on()
                uid = self._mifare.read_uid()
                if uid is None:
                    last_uid = uid
                    continue
                if last_uid == uid:
                    self.logger.log(logging.DEBUG, "Read the same card")
                    continue
                self.green_led.off()
                self.red_led.on()
                self.logger.log(logging.DEBUG, "Start reading block")
                data = self._mifare.read_block(
                    uid,
                    self._config[CONFIG]['BLOCK_NUMBER'],
                    PN532SPI.MIFARE_CMD_AUTH_B,
                    self._config[CONFIG]['KEY_B'])
                self.logger.log(logging.DEBUG, "Read block")
                if data is None:
                    self.logger.log(logging.DEBUG, "Data is None")
                    continue
                self.logger.log(logging.DEBUG, "Data in block: " + str(binascii.hexlify(data[:16])))

                #sending get request
                card_id = data[12] << 24 | data[13] << 16 | data[14] << 8 | data[15]
                # 1 - out | 0 - in
                direction = 1 if self.mode else 0
                self.logger.log(logging.DEBUG, str(card_id))
                params = {
                    'key': self._config[CONFIG]['APIKEY'],
                    'card_id': card_id,
                    'dir': direction,
                    'time': 21}
                r = requests.get(url = self._config[CONFIG]['URL'], params=params)
                self.logger.log(logging.INFO, "Sending request: " + r.url)
                if r.ok and r.content is 0:
                    self.logger.log(logging.INFO, "Sent request.")
                    self.buzzer.on()
                    time.sleep(0.3)
                    self.buzzer.off()
                    last_uid = uid
                    Program.mode = False
                    Program.blue_led.off()
                else:
                    self.logger.log(logging.ERROR, "Can't connect to server. Status code: {0}".format(r.status_code))
            except Exception as ex:
                self.logger.log(logging.ERROR, ex)

    def setup_pins(self):
        GPIO.setwarnings(False)
        GPIO.setmode(GPIO.SUNXI)

    def exit_handler(self, first, second):
        sys.exit(0)

class Mifare(object):
    def __init__(self, CS, MISO, MOSI, SCLK, logger):
        self._pn532 = PN532SPI.PN532(CS, SCLK, MOSI, MISO)
        self.logger = logger

    def setup(self):
        self._pn532.begin()
        # Configure PN532 to communicate with MiFare cards.
        self._pn532.SAM_configuration()
        # Get the firmware version from the chip and print(it out.)
        ic, ver, rev, support = self._pn532.get_firmware_version()
        self.logger.log(logging.DEBUG, 'Found PN532 with firmware version: {0}.{1}'.format(ver, rev))

    def read_uid(self):
        return self._pn532.read_passive_target()

    def read_block(self, uid, block_number, auth_type, key):
        if not self._pn532.mifare_classic_authenticate_block(uid, block_number, auth_type, key):
            return None

        return self._pn532.mifare_classic_read_block(block_number)

class OutputDevice(object):
    HIGH = True
    LOW = False

    def __init__(self, channel, on_state):
        self._channel = channel
        self._on_state = on_state
        GPIO.setup(channel, GPIO.OUT)
        self._state = not on_state
        self.off()

    def on(self):
        GPIO.output(self._channel, self._on_state)
        self._state = self._on_state

    def off(self):
        GPIO.output(self._channel, not self._on_state)
        self._state = not self._on_state

    def toggle(self):
        GPIO.output(self._channel, not self._state)
        self._state = not self._state

class Logger(object):
    def __init__(self, logger, level):
        self.logger = logger
        self.level = level

    def write(self, message):
        if message.rstrip() != "":
            self.logger.log(self.level, message.rstrip())

