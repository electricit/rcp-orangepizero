#!/bin/sh
USERNAME=root

if [ "$(whoami)" != $USERNAME ]; then
        echo "Script must be run as user: $USERNAME"
        exit 1
fi

# sudo apt-get update
# sudo apt-get install python 3.6
# curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
# python3 get-pip.py

# pip3 install --upgrade configparser
# pip3 install --upgrade OPi.GPIO
pip3 install --upgrade requests

rm /etc/init.d/rcpservice.sh
cp rcpservice.sh /etc/init.d
chmod u+rwx /etc/init.d/rcpservice.sh

update-rc.d rcpservice.sh defaults

cd ..
rm -r /usr/local/bin/rcp
cp rcp -R /usr/local/bin/
chmod u+rwx /usr/local/bin/rcp/rcp_runner.py
